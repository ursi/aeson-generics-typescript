# Aeson Generics TypeScript

This project generates TypeScript type definitions using GHC.Generics, that match Aeson instances generated the same way.

```haskell
data Foo = ...
  deriving stock (Generic)
  deriving anyclass (ToJSON, FromJSON, TypeScriptDefinition)
```

Is all it takes to have your TypeScript type definition for your data type. Now you can obtain the TypeScript definition as a string like so.

```haskell
fooTSDef :: String
fooTSDef = getPrintedDefinition @Foo
```

## Example

You can see many examples in the tests. One provided here for documentation purposes:

```haskell
data Sum a b = Foyst a
             | Loser b
  deriving stock (Eq, Generic, Ord, Show)
  deriving anyclass (ToJSON, TypeScriptDefinition)

-- getPrintedDefinition must have a concrete type. If you want to retain
-- generic type variables in TypeScript, use `TSGenericVar` to make the
-- type concrete.
sumTSDef :: String
sumTSDef = getPrintedDefinition @(Sum (TSGenericVar "a") (TSGenericVar "b"))
```

will generate

```TypeScript
// Defined in Data.Aeson.Generics.TypeScript.Types of main
export type Sum<A,B> = Foyst<A> | Loser<B>;
interface Foyst<A> {
  readonly tag: "Foyst";
  readonly contents: A;
}
export interface Loser<B> {
  readonly tag: "Loser";
  readonly contents: B;
}
```
