{
  description = "Aeson Generic TypeScript";

  nixConfig = {
    extra-substituters = "https://horizon.cachix.org";
    extra-trusted-public-keys = "horizon.cachix.org-1:MeEEDRhRZTgv/FFGCv3479/dmJDfJ82G6kfUDxMSAw0=";
  };

  inputs = {
    flake-utils.url = "github:numtide/flake-utils";
    horizon-platform.url = "git+https://gitlab.horizon-haskell.net/package-sets/horizon-platform";

    lint-utils = {
      url = "github:homotopic/lint-utils";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    nixpkgs.url = "github:nixos/nixpkgs/nixpkgs-unstable";
    shelpers.url = "gitlab:platonic/shelpers";
  };

  outputs =
    inputs@
    { self
    , flake-utils
    , lint-utils
    , horizon-platform
    , nixpkgs
    , ...
    }: with builtins; let
      inherit (inputs.nixpkgs) lib;
      onlyHaskell = fs2source
        (fs.union
          (onlyExts-fs [ "cabal" "hs" "project" ] ./.)
          ./LICENSE # horizon requires this file to build
        );
      fs = lib.fileset;
      fs2source = fs': path: fs.toSource { root = path; fileset = fs'; };
      onlyExts-fs = exts: fs.fileFilter (f: foldl' lib.or false (map f.hasExt exts));
      onlyExts = exts: path: fs2source (onlyExts-fs exts path) path;

    in
    {
      overlays.default = final: prev: {
        aeson-generics-typescript = final.callCabal2nix "aeson-generics-typescript" (onlyHaskell ./.) { };
      };
    } //
    flake-utils.lib.eachSystem [
      "x86_64-linux"
      "aarch64-linux"
      "x86_64-darwin"
      "aarch64-darwin"
    ]
      (system:
      let
        pkgs = import nixpkgs { inherit system; };
        inherit (pkgs) lib;
        hlib = pkgs.haskell.lib;

        legacyPackages = horizon-platform.legacyPackages.${system}.extend self.overlays.default;
        inherit (inputs.shelpers.lib pkgs) eval-shelpers shelp;
        shelpers = eval-shelpers [
          ({ config, ... }: {
            shelpers."."."General Functions" = {
              format = {
                description = "format code";
                make-app = true;
                script = ''
                  nix fmt
                  stylish-haskell -ir .
                  cabal-fmt -i *.cabal
                '';
              };

              shelp = shelp config;
            };
          })
        ];
      in
      {
        packages.default = hlib.setBuildTarget legacyPackages.aeson-generics-typescript "lib:aeson-generics-typescript";
        devShells.default = legacyPackages.aeson-generics-typescript.env.overrideAttrs (attrs: {
          buildInputs = attrs.buildInputs ++ (with pkgs; [
            cabal-install
            haskellPackages.cabal-fmt
            legacyPackages.ghcid
            legacyPackages.hlint
            legacyPackages.haskell-language-server
            nixpkgs-fmt
            stylish-haskell
            nodePackages.typescript
          ]);

          shellHook = ''
            ${shelpers.functions}
            shelp
          '';
        });

        checks = let lu = lint-utils.linters.${system}; in {
          cabal-formatting = lu.cabal-fmt { src = onlyExts [ "cabal" ] ./.; };
          haskell-warnings = lu.werror { pkg = legacyPackages.aeson-generics-typescript; };
          haskell-formatting = lu.stylish-haskell {
            src = fs2source
              (fs.union (onlyExts-fs [ "hs" ] ./.) ./.stylish-haskell.yaml)
              ./.;
          };
          haskell-linting = lu.hlint {
            src = fs2source
              (fs.union (onlyExts-fs [ "hs" ] ./.) ./.hlint.yaml)
              ./.;
          };

          nix-formatting = lu.nixpkgs-fmt { src = onlyExts [ "nix" ] ./.; };

          tests = pkgs.runCommand "test.sh"
            {
              buildInputs = [
                (hlib.justStaticExecutables legacyPackages.aeson-generics-typescript)
                pkgs.nodePackages.typescript
              ];
            } ''
            tests
            echo 0 > $out
          '';
        };

        formatter = pkgs.nixpkgs-fmt;
        inherit (shelpers) apps;
        shelpers = shelpers.files;
      });
}
