cabal-version:      3.0
name:               aeson-generics-typescript
version:            0.0.0.1
synopsis:
  Generates TypeScript definitions that match Generic Aeson encodings

description:
  This project uses GHC.Generics to generate TypeScript type definitions that match the Generic Aseon encoding for the same type.
  Included here are tests that round trip by compling the TypeScript with tsc, and checking that generated values type check.

category:           Web
author:             Platonic.Systems
maintainer:         info@Platonic.Systems
copyright:          2023
license:            BSD-3-Clause
license-file:       LICENSE
build-type:         Simple
extra-source-files: README.md

source-repository head
  type:     git
  location: https://gitlab.com/platonic/aeson-generics-typescript.git

common shared
  default-language:   GHC2021
  default-extensions:
    AllowAmbiguousTypes
    ApplicativeDo
    BlockArguments
    CPP
    DataKinds
    DefaultSignatures
    DeriveAnyClass
    DerivingStrategies
    DerivingVia
    DuplicateRecordFields
    ExtendedDefaultRules
    FunctionalDependencies
    GADTs
    ImpredicativeTypes
    LambdaCase
    LinearTypes
    MultiWayIf
    OverloadedLabels
    OverloadedStrings
    PartialTypeSignatures
    PatternSynonyms
    QuantifiedConstraints
    RecordWildCards
    RoleAnnotations
    TypeFamilies
    TypeFamilyDependencies
    UndecidableInstances
    ViewPatterns

library
  import:          shared
  hs-source-dirs:  src
  exposed-modules: Data.Aeson.Generics.TypeScript
  hs-source-dirs:  src
  ghc-options:
    -Weverything -Wno-all-missed-specialisations -Wno-implicit-prelude
    -Wno-missing-local-signatures -Wno-missing-import-lists
    -Wno-missing-export-lists -Wno-missing-safe-haskell-mode
    -Wno-prepositive-qualified-module -Wno-safe -Wno-unsafe

  build-depends:
    , base
    , containers
    , string-interpolate
    , text
    , time

executable tests
  import:         shared
  main-is:        Main.hs
  hs-source-dirs: test src
  ghc-options:
    -Wall -Wcompat -fwarn-redundant-constraints
    -fwarn-incomplete-uni-patterns -fwarn-tabs
    -fwarn-incomplete-record-updates -fwarn-identities -threaded
    -fno-warn-missing-home-modules -rtsopts -freduction-depth=1000
    -with-rtsopts=-N +RTS -H2G -A32M -RTS

  build-depends:
    , aeson               >=2.1.2  && <2.2
    , base                >=4.17.2 && <4.18
    , bytestring          >=0.11.5 && <0.12
    , containers          >=0.6.7  && <0.7
    , directory           >=1.3.7  && <1.4
    , filepath            >=1.4.2  && <1.5
    , process             >=1.6.17 && <1.7
    , QuickCheck          >=2.14.3 && <2.15
    , random              >=1.2.1  && <1.3
    , split               >=0.2.4  && <0.3
    , string-interpolate  >=0.3.2  && <0.4
    , sydtest             >=0.15   && <0.16
    , sydtest-discover
    , text                >=2.0.2  && <2.1
    , time                >=1.12.2 && <1.13

  other-modules:
    Data.Aeson.Generics.TypeScript
    Data.Aeson.Generics.TypeScript.AesonSpec
    Data.Aeson.Generics.TypeScript.ASTSpec
    Data.Aeson.Generics.TypeScript.PrintSpec
    Data.Aeson.Generics.TypeScript.Types
