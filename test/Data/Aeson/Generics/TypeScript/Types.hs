{-# OPTIONS_GHC -fno-warn-orphans #-}
module Data.Aeson.Generics.TypeScript.Types where

import           Data.Aeson (ToJSON)
import           Data.Aeson.Generics.TypeScript
  ( FieldTypeName
  , TypeScriptDefinition
  )
import           Data.Kind (Type)
import           Data.Map (Map)
import           Data.Time.Clock (UTCTime)
import           GHC.Generics (Generic)

definedIn :: String
definedIn = "Defined in Data.Aeson.Generics.TypeScript.Types of main"

newtype GotTime = GotTime { unGotTime :: UTCTime }
  deriving stock (Generic)
  deriving anyclass (ToJSON, TypeScriptDefinition)

data Unit
  = MkUnit
  deriving stock (Generic)
  deriving anyclass (ToJSON, TypeScriptDefinition)

data CouldBe a = ForSure a
               | Nah
  deriving stock (Eq, Generic, Ord, Show)
  deriving anyclass (ToJSON, TypeScriptDefinition)

data Sum a b = Foyst a
             | Loser b
  deriving stock (Eq, Generic, Ord, Show)
  deriving anyclass (ToJSON, TypeScriptDefinition)

data HasEither = HasEither
  { notTheEither :: Int
  , theEither    :: Either String Bool
  }
  deriving stock (Eq, Generic, Ord, Show)
  deriving anyclass (ToJSON, TypeScriptDefinition)

data Prod a b c = MkProd a b c
  deriving stock (Eq, Generic, Ord, Show)
  deriving anyclass (ToJSON, TypeScriptDefinition)

data ItsEnum
  = One
  | Two
  | Three
  deriving stock (Eq, Generic, Ord, Show)
  deriving anyclass (ToJSON, TypeScriptDefinition)

data ItsRecord = MkItsRecord
  { oneThing   :: Int
  , twoThing   :: String
  , threeThing :: ()
  }
  deriving stock (Eq, Generic, Ord, Show)
  deriving anyclass (ToJSON, TypeScriptDefinition)

data ItsRecordWithGeneric a = MkItsRecordWithGeneric
  { oneThing   :: Int
  , twoThing   :: Maybe String
  , threeThing :: a
  }
  deriving stock (Eq, Generic, Ord, Show)
  deriving anyclass (ToJSON, TypeScriptDefinition)

data GenericRecordInSum a = OtherThing
                          | MkGenericRecordInSum
  { oneThing :: Int
  , twoThing :: [a]
  }
  deriving stock (Eq, Generic, Ord, Show)
  deriving anyclass (ToJSON, TypeScriptDefinition)

type NewString :: Type
newtype NewString = MkNewString String
  deriving newtype (Eq, FieldTypeName, Generic, Ord, Show, ToJSON)

type RecordWithWrappedType :: Type
data RecordWithWrappedType = RecordWithWrappedType
  { oneThing :: Int
  , twoThing :: [NewString]
  }
  deriving stock (Eq, Generic, Ord, Show)
  deriving anyclass (ToJSON, TypeScriptDefinition)

type NewIdentity :: Type -> Type
newtype NewIdentity a = NewIdentity a
  deriving stock (Eq, Generic, Ord, Show)
  deriving anyclass (ToJSON, TypeScriptDefinition)

type MapParty :: Type -> Type -> Type
newtype MapParty a b = MapParty (Map a b)
  deriving stock (Eq, Generic, Ord, Show)
  deriving anyclass (ToJSON, TypeScriptDefinition)
