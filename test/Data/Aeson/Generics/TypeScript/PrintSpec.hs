{-# LANGUAGE QuasiQuotes #-}

module Data.Aeson.Generics.TypeScript.PrintSpec
  ( main
  , spec
  ) where

import           Data.Aeson.Generics.TypeScript
  ( TSGenericVar
  , TypeScriptDefinition (..)
  , printTS
  )
import           Data.Aeson.Generics.TypeScript.Types
  ( CouldBe
  , GenericRecordInSum
  , GotTime
  , HasEither
  , ItsEnum
  , ItsRecord
  , ItsRecordWithGeneric
  , NewIdentity
  , Prod
  , RecordWithWrappedType
  , Sum
  , Unit
  , definedIn
  )
import           Data.String.Interpolate (i)
import           Test.Syd (Spec, it, shouldBe, sydTest)

main :: IO ()
main = sydTest spec

spec :: Spec
spec = do
  it "Unit" $ printTS (gen @Unit) `shouldBe`
      [i|// #{definedIn}
export type Unit = [];|]

  it "GotTime" $ printTS (gen @GotTime) `shouldBe`
    [i|// #{definedIn}
export interface GotTime {
  // readonly tag: "GotTime";
  readonly unGotTime: string;
}|]

  it "CouldeBe a" $ printTS (gen @(CouldBe (TSGenericVar "a"))) `shouldBe`
      [i|// #{definedIn}
export type CouldBe<A> = ForSure<A> | Nah;
export interface ForSure<A> {
  readonly tag: "ForSure";
  readonly contents: A;
}
export interface Nah {
  readonly tag: "Nah";
}|]

  it "CouldeBe Int" $ printTS (gen @(CouldBe Int)) `shouldBe`
      [i|// #{definedIn}
export type CouldBe = ForSure | Nah;
export interface ForSure {
  readonly tag: "ForSure";
  readonly contents: number;
}
export interface Nah {
  readonly tag: "Nah";
}|]

  it "Sum () String" $ printTS (gen @(Sum () String)) `shouldBe`
      [i|// #{definedIn}
export type Sum = Foyst | Loser;
export interface Foyst {
  readonly tag: "Foyst";
  readonly contents: [];
}
export interface Loser {
  readonly tag: "Loser";
  readonly contents: string;
}|]

  it "Sum a b" $ printTS (gen @(Sum (TSGenericVar "a") (TSGenericVar "b"))) `shouldBe`
      [i|// #{definedIn}
export type Sum<A,B> = Foyst<A> | Loser<B>;
export interface Foyst<A> {
  readonly tag: "Foyst";
  readonly contents: A;
}
export interface Loser<B> {
  readonly tag: "Loser";
  readonly contents: B;
}|]

  it "Sum a a" $ printTS (gen @(Sum (TSGenericVar "a") (TSGenericVar "a"))) `shouldBe`
      [i|// #{definedIn}
export type Sum<A> = Foyst<A> | Loser<A>;
export interface Foyst<A> {
  readonly tag: "Foyst";
  readonly contents: A;
}
export interface Loser<A> {
  readonly tag: "Loser";
  readonly contents: A;
}|]

  it "Prod a b" $ printTS (gen @(Prod (TSGenericVar "a") (TSGenericVar "b") (TSGenericVar "c"))) `shouldBe`
      [i|// #{definedIn}
export type Prod<A,B,C> = [A, B, C];|]

  it "Prod a a" $ printTS (gen @(Prod (TSGenericVar "a") (TSGenericVar "a") (TSGenericVar "a"))) `shouldBe`
      [i|// #{definedIn}
export type Prod<A> = [A, A, A];|]

  it "ItsEnum" $ printTS (gen @ItsEnum) `shouldBe`
      [i|// #{definedIn}
export type ItsEnum = "One" | "Two" | "Three";|]

  it "ItsRecord" $ printTS (gen @ItsRecord) `shouldBe`
      [i|// #{definedIn}
export interface ItsRecord {
  // readonly tag: "MkItsRecord";
  readonly oneThing: number;
  readonly twoThing: string;
  readonly threeThing: [];
}|]

  it "ItsRecordWithGeneric" $ printTS (gen @(ItsRecordWithGeneric (TSGenericVar "a"))) `shouldBe`
      [i|// #{definedIn}
export interface ItsRecordWithGeneric<A> {
  // readonly tag: "MkItsRecordWithGeneric";
  readonly oneThing: number;
  readonly twoThing: string | null;
  readonly threeThing: A;
}|]

  it "RecordWithWrappedType" $ printTS (gen @RecordWithWrappedType) `shouldBe`
      [i|// #{definedIn}
export interface RecordWithWrappedType {
  // readonly tag: "RecordWithWrappedType";
  readonly oneThing: number;
  readonly twoThing: Array<string>;
}|]

  it "GenericRecordInSum" $ printTS (gen @(GenericRecordInSum (TSGenericVar "a"))) `shouldBe`
      [i|// #{definedIn}
export type GenericRecordInSum<A> = OtherThing | MkGenericRecordInSum<A>;
export interface OtherThing {
  readonly tag: "OtherThing";
}
export interface MkGenericRecordInSum<A> {
  readonly tag: "MkGenericRecordInSum";
  readonly oneThing: number;
  readonly twoThing: Array<A>;
}|]

  it "HasEither" $ printTS (gen @HasEither) `shouldBe`
      [i|// #{definedIn}
export interface HasEither {
  // readonly tag: "HasEither";
  readonly notTheEither: number;
  readonly theEither: { Left: string } | { Right: boolean };
}|]

  it "NewIdentity" $ printTS (gen @(NewIdentity (TSGenericVar "a"))) `shouldBe`
      [i|// #{definedIn}
export type NewIdentity<A> = A;|]
